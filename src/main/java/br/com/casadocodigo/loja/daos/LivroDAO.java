package br.com.casadocodigo.loja.daos;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.casadocodigo.loja.model.Livro;

public class LivroDAO {

	@PersistenceContext
	private EntityManager manager;	
	
	public LivroDAO() {
		
	}
	
	
	public void salvar(Livro livro){
		manager.persist(livro);
	}

}
