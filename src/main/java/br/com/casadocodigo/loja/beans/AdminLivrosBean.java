package br.com.casadocodigo.loja.beans;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import br.com.casadocodigo.loja.daos.LivroDAO;
import br.com.casadocodigo.loja.model.Livro;

@Named
@RequestScoped
public class AdminLivrosBean {
	
	private Livro livro;
	
	@Inject
	private LivroDAO livroDAO;
	
	@PostConstruct
	public void init(){
		livro = new Livro();
	}
	
	@Transactional
	public void salvar(){
		livroDAO.salvar(this.livro);
		System.out.println("Livro salvo com sucesso!");
		System.out.println("Livro cadastrado: " +getLivro());
		livro = new Livro();
	}

	public Livro getLivro() {
		return livro;
	}

	public void setLivro(Livro livro) {
		this.livro = livro;
	}

}
